#*************************************************
#       Detecting Question type
#*************************************************

# Take in a tokenized question and return the question type and body
def processquestion(qwords):
    
    # Find "question word" (what, who, where, etc.)
    questionword = ""
    qidx = -1
   
    for (idx, word) in enumerate(qwords):
        if word.lower() in questionwords:
            questionword = word.lower()
            qidx = idx
            break
        elif word.lower() in yesnowords:
            return ("YESNO", qwords)


    if qidx < 0:
        return ("MISC", qwords)

    if qidx > len(qwords) - 3:
        target = qwords[:qidx]
    else:
        target = qwords[qidx+1:]
    type = "MISC"
##############################>    

#    # Determine question type


    if questionword in ["who", "whose", "whom"]:
        type = "PERSON"
    if questionword == "what":
        type = "DESC"
    elif questionword == "where":
        type = "LOCATION"
    elif questionword == "when":
        type = "TIME"
    elif questionword == "how":
        type="NUM"
        if target[0] in ["far","few", "little", "much", "many","tall","short"]:
            type = "QUANTITY"
            target = target[1:]
        elif target[0] in ["young", "old", "long"]:
            type = "TIME"
            target = target[1:]
#
#    # Trim possible extra helper verb
    if questionword == "which":
        
        target = target[1:]

    if target[0] in yesnowords:
        target = target[1:]
#    
#    # Return question data
    return (type, target)
#**************************!*********************************** Function ended