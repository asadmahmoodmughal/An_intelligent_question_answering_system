\select@language {english}
\contentsline {chapter}{\numberline {1}Preliminaries and Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Problem Statement}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Solution}{1}{section.1.2}
\contentsline {section}{\numberline {1.3}Objective}{2}{section.1.3}
\contentsline {section}{\numberline {1.4}Product Scope}{2}{section.1.4}
\contentsline {section}{\numberline {1.5}Product Features}{2}{section.1.5}
\contentsline {section}{\numberline {1.6}Intended Audience and Reading Suggestions}{3}{section.1.6}
\contentsline {chapter}{\numberline {2}Review of Literature}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Existing Techniques for Question Answering Systems}{5}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Information Retrieval (IR) Based Question Answering System}{5}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Knowledge Based Question Answering}{6}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}A Hybrid of Information Retrieval System and Knowledge based systems}{8}{subsection.2.1.3}
\contentsline {section}{\numberline {2.2}Answer Type Taxonomy}{10}{section.2.2}
\contentsline {section}{\numberline {2.3}Simple Protocol and RDF Query Language(SPARQL)}{11}{section.2.3}
\contentsline {section}{\numberline {2.4}DBPedia}{12}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Advantages of using DBPedia}{12}{subsection.2.4.1}
\contentsline {section}{\numberline {2.5}Summarization}{13}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}Types of Text Summarization Approaches}{13}{subsection.2.5.1}
\contentsline {section}{\numberline {2.6}Cosine and Jaccard Similarity}{14}{section.2.6}
\contentsline {section}{\numberline {2.7}Answer Extraction Approaches}{14}{section.2.7}
\contentsline {chapter}{\numberline {3}Proposed System}{17}{chapter.3}
\contentsline {section}{\numberline {3.1}Requirements}{17}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Software Requirements}{18}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Hardware Requirements}{18}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}User End Requirements}{18}{subsection.3.1.3}
\contentsline {section}{\numberline {3.2}System Description}{19}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}System Phases}{19}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Question Processing}{20}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Summarization and Passage Retrieval:}{24}{subsection.3.2.3}
\contentsline {section}{\numberline {3.3}Answer Extraction}{24}{section.3.3}
\contentsline {section}{\numberline {3.4}Use case Diagram \& Description}{26}{section.3.4}
\contentsline {section}{\numberline {3.5}Activity Diagram}{27}{section.3.5}
\contentsline {section}{\numberline {3.6}System Sequence Diagram}{28}{section.3.6}
\contentsline {chapter}{\numberline {4}Conclusions and Future Work}{29}{chapter.4}
\contentsline {section}{\numberline {4.1}Conclusion}{29}{section.4.1}
\contentsline {section}{\numberline {4.2}Future Work}{30}{section.4.2}
\contentsline {chapter}{References}{31}{chapter*.23}
