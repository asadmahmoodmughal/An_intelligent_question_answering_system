%
% FAST-NUCES PhD Thesis Class v1.0
% By Omar Usman Khan <omar.khan@nu.edu.pk
%
% Version History
% 1.0 2007-05-21

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{fast-nuces-ms}[2018/01/01 v1.0 FAST-NUCES-MS Thesis Class]

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{book}}
\ProcessOptions\relax
\LoadClass[12pt, twoside, a4paper]{book}

\RequirePackage{amssymb}
\RequirePackage{amsmath}
\RequirePackage{amsthm}
\RequirePackage{pgfplots}
\RequirePackage{tikz}
\RequirePackage[sort, numbers, authoryear]{natbib}
\RequirePackage{emptypage}
\RequirePackage{graphics}
\RequirePackage{graphicx}
\RequirePackage{fancyhdr}
\RequirePackage{eucal}
\RequirePackage{soul}
\RequirePackage[english]{babel}
%\RequirePackage[usenames, dvipsnames]{color}
\RequirePackage{etoolbox}
\RequirePackage[perpage]{footmisc}
\RequirePackage{ifthen}
\RequirePackage{ifpdf}
\RequirePackage{nomencl}
\RequirePackage[utf8]{inputenc}
\RequirePackage{epsfig}
\RequirePackage[lmargin=2.5cm,rmargin=2.5cm,
				tmargin=2.5cm,bmargin=2.5cm,
				includefoot,bindingoffset=1cm]{geometry}
\RequirePackage[bookmarks,
                bookmarksopen = true,
                bookmarksnumbered = true,
                breaklinks = true,
                linktocpage,
                pagebackref = false,
                ocgcolorlinks,
				colorlinks = true,
                linkcolor = blue,
                urlcolor  = blue,
                citecolor = red,
                anchorcolor = green,
                hyperindex = true,
                hyperfigures
                ]{hyperref}
% DELETE
%\RequirePackage{showframe}
% END DELETE

%Bibliography
\renewcommand{\bibname}{References}

% Nomenclature
\makeglossary
\renewcommand\nomgroup[1]{%
  \ifthenelse{\equal{#1}{S}}{%
   	\item[\textbf{Symbols}] % S Symbols
  }{
  	\ifthenelse{\equal{#1}{A}}{%
    	\item[\textbf{Abbreviations}] % A Abbreviations
	}{%             G - Greek
    	\ifthenelse{\equal{#1}{C}}{%
        	\item[\textbf{Constants}] % C Constants
		}{
        	\ifthenelse{\equal{#1}{R}}{%
           		\item[\textbf{Rates}] % R Rates
			}{
             {}
			}
		 }
	  }
    }
  }
\makenomenclature

%%%%%%%%%%%%%%%%%% General Settings
\linespread{1}
\setlength{\parindent}{0cm}
\setlength{\parskip}{0.5em}
\setcounter{secnumdepth}{3} % For TOC
\setcounter{tocdepth}{3}
\pdfcompresslevel=9
\DeclareGraphicsExtensions{.png, .jpg, .jpeg, .pdf, .eps, .ps}
\pgfplotsset{compat=1.11}
%%%%%%%%%%%%%%%%%%
\newcommand{\signedby}[1]{%
	\vspace{2cm}
	\par\noindent\hspace{7.5cm}\hrulefill\par
	\par\noindent\hspace{7.5cm}{\large #1}\par
	\vspace{0cm}
	\par\noindent\hspace{7.5cm}{\large Dated: }\par
}
\newcommand{\longsignedby}[1]{%
	\par\hspace{.2cm}\begin{minipage}[t]{.55\columnwidth}
	\noindent#1
	\end{minipage}
	\hfill
	\begin{minipage}[t]{.35\columnwidth}
		Signature: \hrulefill
	\end{minipage}
}
%%%%%%%%%%%%%%%%%%
\newcommand{\yellow}[1]{\hl{#1}}
\newcommand{\comments}[1]{} 
%%%%%%%%%%%%%%%%%%
%\AtBeginDocument{Hello}%\maketitle}
\AfterEndPreamble{\maketitle}
%%%%%%%%%%%%%%%%%%

\pagestyle{fancy}
\renewcommand{\chaptermark}[1]{\markboth{\thechapter. #1 }{}}
\renewcommand{\sectionmark}[1]{\markright{\thesection\ #1}}
\fancyhf{}
\fancyhead[RO]{\footnotesize\rightmark}
\fancyhead[LE]{\footnotesize\leftmark}
\fancyfoot[C]{\thepage}
\fancypagestyle{plain}{
  \fancyhead{}
  \renewcommand{\headrulewidth}{0pt}
}

\newcommand{\submittedtext}{{A thesis submitted for the degree of}}

% Experiment With This:
\renewcommand{\@makechapterhead}[1]{%
  \vspace*{60\p@}%
  {\parindent \z@ \raggedright \normalfont
    \vspace{5pt}%                                 % add vertical space
    \ifnum \c@secnumdepth >\m@ne
        \Large\bfseries\scshape \@chapapp\space \thechapter % Chapter number
        \par\nobreak
        \vskip 20\p@
    \fi
    \interlinepenalty\@M
    \Huge\bfseries \scshape #1\par                         % chapter title
    \nobreak
    \vskip 40\p@
  }}

% DECLARATIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The Roll Number of Student
\def\rollnumber#1{\gdef\@rollnumber{#1}}
% The year and term the degree will be officially conferred
\def\degreeyear#1{\gdef\@degreeyear{#1}}
\def\degreemonth#1{\gdef\@degreemonth{#1}}
% The full (unabbreviated) name of the degree
\def\degree#1{\gdef\@degree{#1}}
% The name of your college or department
\def\department#1{\gdef\@department{#1}}
\def\faculty#1{\gdef\@faculty{#1}}
% The name of your University
\def\university#1{\gdef\@university{#1}}
% Defining the crest
\def\crest#1{\gdef\@crest{#1}}
% Supervisor Name
\def\supervisor#1{\gdef\@supervisor{#1}}
% Degree Name
\def\degreename#1{\gdef\@degreename{#1}}
% Campus City
\def\campuscity#1{\gdef\@campuscity{#1}}
% Committee Examination Names
\def\internalexaminer#1{\gdef\@internalexaminer{#1}}
\def\externalexaminer#1#2{\gdef\@externalexaminer{#1\par\hspace{0cm}#2}}
% Dean Names
\def\deanname#1{\gdef\@deanname{#1}}
% Director Name
\def\directorname#1{\gdef\@directorname{#1}}
% HoD Name
\def\hodname#1{\gdef\@hodname{#1}}
% GPC Coordinator Name
\def\gpcname#1{\gdef\@gpcname{#1}}

%%%%%%%%%%%%%%%%%%%%%%%%%
% Some Important Environments
\newenvironment{alwayssingle}{%
       \@restonecolfalse\if@twocolumn\@restonecoltrue\onecolumn
       \else\newpage\fi}
       {\if@restonecol\twocolumn\else\newpage\fi}

%%%%%%%%%%%
% Title Page Template
\renewcommand{\maketitle}{%
\begin{alwayssingle}
	\renewcommand{\footnotesize}{\small}
    \renewcommand{\footnoterule}{\relax}
    \thispagestyle{empty}
	
	% Title Page
	\begin{center}
		\par{\fontsize{24}{24}\selectfont \textbf{\@title}}\par
		\vspace{4cm}
		\par{\fontsize{21}{21}\selectfont \@author}\par
		\vspace{2cm}
		\par{\fontsize{21}{21}\selectfont \textbf{Bachelors of Sciences}}\par
		\vspace{1cm}
		\par{\fontsize{16}{16}\selectfont A thesis submitted in partial fulfillment\\of the
			requirements for the degree of \emph{Master of Science}\\at the \emph{National University of
			Computer and Emerging Sciences}}\par
		\vspace{1cm}
		\begin{figure}[h]\centering
			\includegraphics[width=.30\columnwidth]{ThesisFigs/FASTLogo}
		\end{figure}		
		\vspace{1cm}
		\par{\fontsize{17}{17}\selectfont \textbf{\@department}}\par
		\vspace{0.5cm}
		\par{\fontsize{17}{17}\selectfont \textbf{National University of Computer and Emerging Sciences, \@campuscity, Pakistan}}\par
		\vspace{0.5cm}
		\par{\fontsize{17}{17}\selectfont \textbf{\@degreeyear}}\par
	\end{center}
  	\null\vfill
	\cleardoublepage

	% Plagiarism Undertaking
	\thispagestyle{empty}
	\vspace{1.5cm}
	{\centering \Large \textbf{Plagiarism Undertaking}\par}
	\vspace{0.5cm}
	I take full responsibility of the research work conducted during the MS Thesis titled ``\@title''. I solemnly declare that the research work presented in the thesis is done solely by me with no significant help from any other person; however, small help wherever taken is duly acknowledged. I have also written the complete thesis by myself. Moreover, I have not presented this thesis (or substantially similar research work) or
	any part of the thesis previously to any other degree awarding institution within Pakistan or abroad.\par
	I understand that the management of {\@department} of National University of Computer and Emerging Sciences has a zero tolerance policy towards plagiarism. Therefore, I, as an author of the above-mentioned thesis, solemnly declare that no portion of my thesis has been plagiarized and any material used in the thesis from other sources is properly referenced. Moreover, the thesis does not contain any literal citing of more than 70 words (total) even by giving a reference unless I have the written permission of the publisher to do so. Furthermore, the work presented in the thesis is my own original work and I have positively cited the related work of the other researchers by clearly differentiating my work from their relevant work.\par
	I further understand that if I am found guilty of any form of plagiarism in my thesis work even after my graduation, the University reserves the right to revoke my MS degree. Moreover, the University will also have the right to publish my name on its website that keeps a record of the students who plagiarized in their thesis work.
	\signedby{\@author}
	\signedby{Verified by Plagiarism Cell Officer}
	\cleardoublepage

	% Author Declaration
	\thispagestyle{empty}
	\vspace{1.5cm}
	{\centering \Large \textbf{Author's Declaration}\par}
	\vspace{0.5cm}
	\par I, \@author, hereby state that my MS thesis titled
	``\@title'' is my own work and it has not been previously submitted by me
	for taking partial or full credit for the award of any degree at this
	University or anywhere else in the world. If my statement is found to be
	incorrect, at any time even after my graduation, the University has the
	right to revoke my MS degree.\par
	\signedby{\@author}
	\cleardoublepage

	% Certificate of Approval
	\thispagestyle{empty}
	\vspace{1.5cm}
	{\centering \Large \textbf{Certificate of Approval}\par}
	\vspace{0.5cm}
	\begin{figure}[h]\centering
		\includegraphics[width=.30\columnwidth]{ThesisFigs/FASTLogo}
	\end{figure}
	\par It is certified that the research work presented in this thesis, titled
	``\emph{\@title}'', was conducted by \emph{\@author} under the supervision of
	\emph{\@supervisor}. No part of this thesis has been submitted anywhere else
	for any other degree. The thesis is submitted to the \@department~ in partial
	fulfillment of the requirements for the degree of \emph{Master of Science in
	\@degreename} at the National University of Computer and Emerging Sciences,
	\@campuscity, Pakistan in \@degreemonth, \@degreeyear.
	\par\vspace{.25cm}
	\par \textbf{Candidate}
	\longsignedby{\@author}
	\par \textbf{Internal Examiner}
	\longsignedby{\@internalexaminer}
	\par\vspace{.15cm}
	\par\textbf{External Examiner}
	\longsignedby{\@externalexaminer}
	\par
	\vspace{1.5cm}
	\begin{center}
	\par \rule{0.5\textwidth}{.4pt}
	\par \@gpcname
	\par Graduate Program Coordinator\\National University of Computer and Emerging Sciences, \@campuscity
	\vspace{1.5cm}
	\par \rule{0.5\textwidth}{.4pt}
	\par \@hodname
	\par HoD of \@faculty \\National University of Computer and Emerging Sciences
	\end{center}
	\cleardoublepage

\end{alwayssingle}}

\newenvironment{dedication}{
  	\pagestyle{empty}
	\begin{alwayssingle}
		\begin{center}
			\vspace*{1.5cm}
			{\LARGE }
		\end{center}
		\vspace{0.5cm}
		\begin{center}
}{
		\end{center} 
	\end{alwayssingle}
	\cleardoublepage
}

\newenvironment{acknowledgements}{
	\pagenumbering{roman}
	\setcounter{page}{1}
	\begin{alwayssingle}
		\begin{center}
			\vspace*{1.5cm}
			{\Large \bfseries Acknowledgements}
		\end{center}
		\vspace{0.5cm}
}{
		\begin{flushright}
			\textbf{\@author}\\
			\textbf{\@rollnumber}
		\end{flushright} 
	\end{alwayssingle}
	\cleardoublepage
}

\newenvironment{abstract} {
	\begin{alwayssingle} 
		\begin{center}
			\vspace*{1.5cm}
			{\Large \bfseries Abstract}
		\end{center}
  		\vspace{0.5cm}
}{
	\end{alwayssingle}
	\cleardoublepage
}

