\select@language {english}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Problem Statement}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Solution}{1}{section.1.2}
\contentsline {section}{\numberline {1.3}Objective}{2}{section.1.3}
\contentsline {section}{\numberline {1.4}Product Scope}{2}{section.1.4}
\contentsline {section}{\numberline {1.5}Product Features}{2}{section.1.5}
\contentsline {section}{\numberline {1.6}Intended Audience and Reading Suggestions}{3}{section.1.6}
\contentsline {chapter}{\numberline {2}Literature Review}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Existing Techniques for Question Answering}{5}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Information Retrieval (IR) Based Question Answering}{5}{subsection.2.1.1}
\contentsline {chapter}{\numberline {3}Conclusions and Future Work}{7}{chapter.3}
\contentsline {chapter}{\numberline {A}Appendix I title}{9}{appendix.A}
\contentsline {chapter}{References}{11}{appendix*.3}
