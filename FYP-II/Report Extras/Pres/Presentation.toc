\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {1}{Project Problem Statement}{3}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {2}{Project Scope}{4}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {3}{Rule Based - Information retrieval QA}{5}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {4}{Bird’s-Eye View Workflow}{6}{0}{4}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {5}{Answer Extraction Via Summary}{7}{0}{5}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {5}{1}{Answer Extraction Via Summary - Flowchart}{7}{0}{5}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {5}{2}{Answer Extraction Via Summary - NP Extraction}{8}{0}{5}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {5}{3}{Answer Extraction Via Summary - Person \& Location Type Words Extraction From Nouns (Results)}{9}{0}{5}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {6}{Answer Extraction Via Key Generation}{10}{0}{6}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {7}{Milestones Achieved}{12}{0}{7}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {8}{FYP - II Timeline}{13}{0}{8}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {9}{References}{14}{0}{9}
