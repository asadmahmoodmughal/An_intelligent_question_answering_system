\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {1}{Project Problem Statement}{3}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {2}{Project Scope}{4}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {3}{Rule Based - Information retrieval QA}{5}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {4}{Bird’s-Eye View Workflow}{6}{0}{4}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {5}{Bird’s-Eye View Workflow - Detailed}{7}{0}{5}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {6}{Answer Extraction Via Summary}{8}{0}{6}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {6}{1}{Answer Extraction Via Summary - Flowchart}{8}{0}{6}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {6}{2}{Answer Extraction Via Summary - NP Extraction}{9}{0}{6}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {6}{3}{Answer Extraction Via Summary - Person \& Location Type Words Extraction From Nouns (Results)}{10}{0}{6}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {7}{Answer Extraction Via Key Generation}{11}{0}{7}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {7}{1}{Literature review for Answer Extraction}{11}{0}{7}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {7}{2}{Key Based}{12}{0}{7}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {7}{3}{XML Based}{13}{0}{7}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {8}{Milestones Achieved}{14}{0}{8}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {9}{References}{15}{0}{9}
