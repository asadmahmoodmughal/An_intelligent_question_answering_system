\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {2}{Project Problem Statement}{4}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {3}{Project Scope}{5}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {4}{Rule Based - Information retrieval QA}{6}{0}{4}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {5}{Bird’s-Eye View Workflow - Detailed}{7}{0}{5}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {6}{Question Processing}{8}{0}{6}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {6}{1}{Natural Language Processing}{8}{0}{6}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {6}{2}{Natural Language Understanding}{10}{0}{6}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {7}{Document Retrieval}{12}{0}{7}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {8}{Answer Extraction}{14}{0}{8}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {8}{1}{Summary}{14}{0}{8}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {8}{2}{Keys + XML}{16}{0}{8}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {9}{Analysis \& Results}{19}{0}{9}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {10}{Use case Diagram}{20}{0}{10}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {11}{Product Features}{21}{0}{11}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {12}{Poster}{22}{0}{12}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {13}{References}{23}{0}{13}
